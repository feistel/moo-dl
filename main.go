package main

import (
	"os"

	"gitlab.com/feistel/moo-dl/moodl"
)

func main() {
	os.Exit(moodl.CLI(os.Args[1:]))
}

