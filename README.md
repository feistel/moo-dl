# moo-dl

moo-dl is a tool for downloading resources from Moodle.

## Description

This script can recursively download resources, courses or entire semesters.  
It does not require an API token.

## Prerequisites

go

## Usage

```
  -m string  url of the moodle instance
  -o string  output dir
  -u string  user
  -p string  password
  -v bool    Verbose
```

