package moodl

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"time"
)

type Student struct {
	Client  http.Client
	Site    url.URL
	Current *Branch
	Conf    Config
}

type Config struct {
	Out     string
	Verbose bool
}

func FromArgs(args []string) (Student, error) {
	fl := flag.NewFlagSet("moodl", flag.ExitOnError)

	verbose := fl.Bool("v", false, "Verbose")
	out := fl.String("o", os.Getenv("OUT"), "Output dir")
	site := fl.String("m", os.Getenv("MOODLE"), "moodle")
	user := fl.String("u", os.Getenv("USER"), "username")
	pass := fl.String("p", os.Getenv("PASS"), "password")

	fl.Parse(args)

	if *site == "" {
		return Student{}, errors.New("Empty moodle instance")
	}

	link, err := url.Parse(*site)
	if err != nil {
		return Student{}, err
	}

	jar, _ := cookiejar.New(nil)

	s := Student{
		Site: *link,
		Conf: Config{
			Out:     *out,
			Verbose: *verbose,
		},
		Client: http.Client{
			Jar:     jar,
			Timeout: time.Minute,
		},
	}

	if err := s.Login(*user, *pass); err != nil {
		return s, err
	}

	return s, nil
}

func (s *Student) Login(user string, pass string) error {
	logintoken, err := s.token()
	if err != nil {
		return err
	}

	rsp, err := s.Client.PostForm(s.Site.String()+"login/index.php", url.Values{
		"username":   {user},
		"password":   {pass},
		"logintoken": {logintoken},
	})

	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	doc, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return err
	}

	if bytes.Contains(doc, []byte("loginerrormessage")) {
		return errors.New("Auth error")
	}

	s.Current = &Home
	return nil
}

func (s *Student) token() (string, error) {
	rsp, err := s.Client.Get(s.Site.String() + "login/index.php")
	if err != nil {
		return "", err
	}

	defer rsp.Body.Close()

	doc, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return "", err
	}

	split := bytes.Split(doc, []byte("name=\"logintoken\" value=\""))
	if len(split) == 1 {
		return "", nil
	}

	token := split[1][0:32]
	return string(token), nil
}

func (s *Student) Choose(choice string) error {
	if choice == ".." {
		s.Current = s.Current.Parent
		return nil
	}

	i, err := strconv.Atoi(strings.TrimSuffix(choice, "d"))
	if err != nil {
		return err
	}

	if i >= len(*s.Current.Children) {
		return nil
	}

	k := (*s.Current.Children)[i]
	if !k.IsViewable() {
		return nil
	}

	if !k.Viewed {
		if k, err = s.View(&k); err != nil {
			return err
		}
		k.Parent = s.Current
		(*s.Current.Children)[i] = k
	}

	if k.Children == nil {
		return nil
	}

	if strings.HasSuffix(choice, "d") {
		log.Printf("Downloading %v", *k.Name)
		if err = s.Download(&k, s.Conf.Out); err != nil {
			return err
		}
		return nil
	}

	s.Current = &k
	return nil

}

func (s *Student) View(b *Branch) (Branch, error) {
	rsp, err := s.Client.PostForm(s.Site.String()+"lib/ajax/getnavbranch.php", url.Values{
		"id":   {*b.Key},
		"type": {strconv.Itoa(int(b.Type))},
	})
	if err != nil {
		return Branch{}, err
	}

	doc, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return Branch{}, err
	}

	defer rsp.Body.Close()
	branch := Branch{}
	if err := json.Unmarshal(doc, &branch); err != nil {
		return Branch{}, err
	}
	branch.Viewed = true
	return branch, nil
}

func (s *Student) Download(b *Branch, out string) error {
	if b.Type == Activity {
		return s.downloadActivity(b, out)
	}

	if b.Type == CourseStructure || b.Type == Course {
		br, err := s.View(b)
		if err != nil {
			return err
		}
		if br.Children == nil {
			if s.Conf.Verbose {
				log.Printf("Skipping empty course structure: %s", *b.Name)
			}
			return nil
		}
		return s.downloadChildren(&br, out)
	}

	if !b.IsViewable() {
		if s.Conf.Verbose {
			log.Printf("Skipping NUL branch")
		}
		return nil
	}
	if s.Conf.Verbose {
		log.Printf("Skipping: %s (unsupported type %v)", *b.Name, b.Type)
	}
	return nil
}

func (s *Student) downloadChildren(b *Branch, out string) error {
	joined := filepath.Join(out, strings.Replace(*b.Name, "/", "-", -1))

	if err := os.Mkdir(joined, 0755); err != nil && !os.IsExist(err) {
		return err
	}

	for _, child := range *b.Children {
		if err := s.Download(&child, joined); err != nil {
			return err
		}
	}

	return nil
}

func (s *Student) downloadActivity(b *Branch, out string) error {
	if *b.Title == "Forum" {
		if s.Conf.Verbose {
			log.Printf("Skipping Forum: %s", *b.Name)
		}
		return nil
	}
	rsp, err := s.Client.Get(*b.Link)
	if err != nil {
		return err
	}

	defer rsp.Body.Close()

	f := path.Base(rsp.Request.URL.Path)

	log.Printf("Downloading %s", f)

	dest, err := os.Create(filepath.Join(out, f))
	if err != nil {
		return err
	}
	defer dest.Close()

	if _, err := io.Copy(dest, rsp.Body); err != nil {
		return err
	}

	return nil
}
