package moodl_test

import (
	"log"
	"math/rand"
	"strconv"
	"testing"
	"time"

	. "gitlab.com/feistel/moo-dl/moodl"
)

func TestView(t *testing.T) {
	s, err := FromArgs([]string{})
	if err != nil {
		t.Fatalf("Runtime error: %v\n", err)
	}

	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 10; i++ {
		log.Printf("=========")
		s.Current.PrintChildren()

		r := rand.Intn(4)
		log.Printf("Choosing %d\n", r)

		if err := s.Choose(strconv.Itoa(r)); err != nil {
			t.Fatalf("Runtime error: %v\n", err)
		}
	}
}
