package moodl

import (
	"encoding/json"
	"log"
)

type Branch struct {
	Name     *string   `json:"name"`
	Type     Type      `json:"type"`
	Key      *string   `json:"key"`
	Title    *string   `json:"title"`
	Link     *string   `json:"link"`
	Children *[]Branch `json:"children"`
	Parent   *Branch   `json:"-"`
	Viewed   bool      `json:"-"`
}

type Type int

// https://github.com/moodle/moodle/blob/fac49f8f7266099eef769ab4d1139f88fa5914d1/lib/navigationlib.php#L48
const (
	Root            Type = 0
	Category        Type = 10
	Course          Type = 20
	CourseStructure Type = 30
	Activity        Type = 40
	User            Type = 80
)

var (
	MyCourses = Branch{
		Name: s("My courses"),
		Key:  s("mycourses"),
		Type: Root,
	}

	Courses = Branch{
		Name: s("Courses"),
		Key:  s("courses"),
		Type: Root,
	}

	Home = Branch{
		Children: &[]Branch{Courses, MyCourses},
		Type:     Root,
	}
)

func s(str string) *string {
	return &str
}

func (br *Branch) IsViewable() bool {
	return br.Key != nil && br.Type != User && br.Type != Activity
}

func (br *Branch) UnmarshalJSON(data []byte) error {
	if len(data) == 2 {
		return nil
	}
	type Alias Branch
	if err := json.Unmarshal(data, (*Alias)(br)); err != nil {
		return err
	}
	return nil
}

func (br *Branch) PrintChildren() {
	if br.Children == nil {
		log.Fatal("Nil children")
	}
	for i, br := range *br.Children {
		if br.Name == nil {
			log.Printf("[%d] NUL\n", i)
		} else {
			log.Printf("[%d] %s (%d)\n", i, *br.Name, br.Type)
		}
	}
}
