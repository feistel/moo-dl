package moodl

import (
	"fmt"
	"log"
)

func CLI(args []string) int {
	s, err := FromArgs(args)
	if err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 2
	}

	if err = runLoop(&s); err != nil {
		log.Fatalf("Runtime error: %v\n", err)
		return 1
	}
	return 0
}

func runLoop(s *Student) error {
	for {
		log.Printf("=========")
		s.Current.PrintChildren()

		var in string
		fmt.Scanf("%s", &in)

		if in == "q" || in == "" {
			return nil
		}

		if err := s.Choose(in); err != nil {
			return err
		}
	}
}

